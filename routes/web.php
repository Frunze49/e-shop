<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/Feedback', [\App\Http\Controllers\Feedback::class, 'Feedback']);

Route::post('/Feedback/submit', [\App\Http\Controllers\Feedback::class, 'GetFeedback'])->name('contact-form');